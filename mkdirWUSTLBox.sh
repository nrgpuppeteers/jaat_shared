#!/bin/bash

BOXDIR=$1
NEWDIR=$2
creds=`head -1 $3`

echo "Starting mkdirWUSTLBox.sh $BOXDIR $NEWDIR"

# Create a new directory 
curl -u $creds --ftp-ssl -v "ftp://ftp.box.com${BOXDIR}" -Q "-MKD ${BOXDIR}${NEWDIR}" --ftp-create-dirs
