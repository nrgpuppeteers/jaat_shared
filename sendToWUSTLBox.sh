#!/bin/bash


UPLOAD=$1
BOXDIR=$2

if [ -z ${CREDPATH} ]; then
   CREDPATH=$3
fi
creds=`head -1 ${CREDPATH}`

echo "Starting sendToWUSTLBox.s $UPLOAD $BOXDIR"

# Upload a file
if [ -s "${UPLOAD}" ]; then
  fileName=`basename $UPLOAD`
  echo "curl -u ${creds} --ftp-ssl -v -T ${UPLOAD} ftps://ftp.box.com${BOXDIR}/${fileName}"
  curl -u ${creds} --ftp-ssl -v -T ${UPLOAD} "ftps://ftp.box.com${BOXDIR}/${fileName}"
else
  echo "No uploads found."
fi

